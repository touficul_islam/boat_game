﻿using UnityEngine;

public class DestroySmallExplotion : MonoBehaviour
{
    public float destroyTimer = 2f;
    private float counter;

    private void Start()
    {
        counter = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        counter += Time.deltaTime;

        while (counter >= destroyTimer)
        {
            counter = 0;
            Destroy(this.gameObject);
        }
    }
}
