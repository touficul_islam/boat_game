﻿using System.Collections;
using UnityEngine;

namespace Unity.Boat
{
    public class BoatGunHelper : MonoBehaviour
    {
        [Header("--Weapon Points--")]
        public CanonController[] rightSideCanons;
        public CanonController[] leftSideCanons;
        [Header("--Weapon Points End--")] 
        
        public int reloadingTime = 3;
        
        
        public Camera mainCamera;
        private CameraMovement cameraMovement;
        private BoatController boatController;

        private bool isDraw ;
        private bool isShoot;
        private bool canShootNow;

        private void Start()
        {
            setIsDraw(false);
            setIsShoot(false);
            setCanShootNow(true);
            cameraMovement = mainCamera.GetComponent<CameraMovement>();
            boatController = GetComponent<BoatController>();
        }

        private void Update()
        {
            mainUpdate();
        }

        private void mainUpdate()
        {
            if (Input.GetMouseButton(1) && boatController.getCurrentSpeed() < boatController.getMaxSpeed() )
            {
                setIsDraw(true);
            }
            else
            {
                setIsDraw(false);
            }

            if (Input.GetMouseButtonDown(0) && isDraw && getCanShootNow() && boatController.getCurrentSpeed() < boatController.getMaxSpeed() )
            {
                setIsShoot(true);
                
                //shake camera
                StartCoroutine(cameraMovement.shakeMeBaby());
                StartCoroutine(Reload());
            }
            else
            {
                setIsShoot(false);
            }
            
            shootCanons();
        }

        private void shootCanons()
        {
            float difference =
                mainCamera.transform.root.transform.eulerAngles.y  - transform.eulerAngles.y;

            difference = HelperFunctions.unwrapAngle(difference);
                
            
            if ( difference <= 80 && difference >= 10 )
            {
                shoot("right");
            }
            else if (difference >= 100 && difference <= 350)
            {
                shoot("left");
            }
        }


        private void shoot(string side)
        {
            CanonController[] canonToActive = { null };
            CanonController[] canonToDeActive = { null };
            
            canonToActive = (side == "right") ? rightSideCanons :
            leftSideCanons;
            
            canonToDeActive = (side != "right") ? rightSideCanons :
                leftSideCanons;
            
            activationCanon( canonToActive );
            activationDrawProjection(canonToActive);
                
            deactivateCanon(canonToDeActive);
            deactivateProject(canonToDeActive);
        }


        private void activationCanon(CanonController[] controllers)
        {
            int length = controllers.Length;
            for (int i = 0; i < length; i++)
            {
                controllers[i].setIsShoot(isShoot);
            }
        }

        private void activationDrawProjection(CanonController[] controllers )
        {
            int length = controllers.Length;
            for (int i = 0; i < length; i++)
            {
                controllers[i].setIsDraw(isDraw);
            }
        }


        private void deactivateCanon(CanonController[] controllers)
        {
            int length = controllers.Length;
            for (int i = 0; i < length; i++)
            {
                controllers[i].setIsShoot(false);
            }
        }


        private void deactivateProject(CanonController[] controllers)
        {
            int length = controllers.Length;
            for (int i = 0; i < length; i++)
            {
                controllers[i].setIsDraw(false);
            }
        }


        public void setCanShootNow(bool value)
        {
            canShootNow = value;
        }


        public bool getCanShootNow()
        {
            return canShootNow;
        }


        public void setIsShoot(bool value)
        {
            isShoot = value;
        }


        public bool getIsShoot()
        {
            return isShoot;
        }


        public void setIsDraw(bool value)
        {
            isDraw = value;
        }


        public bool getIsDraw()
        {
            return isDraw;
        }
        
        IEnumerator Reload()
        {
            setCanShootNow(false);
            yield return new WaitForSeconds(reloadingTime);
            //Stuff after waiting.
            setCanShootNow(true);
        }
    }
}