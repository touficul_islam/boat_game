﻿using System;
using UnityEngine;

namespace Unity.Boat
{
    public class EnemyAI : EnemyAbstract
    {
        public GameObject target;
        
        // set enemy is in front or back
        private float enemyDirection;
        
        //check is the enemy is alive
        private bool isAlive;
        //get enemy helper
        private EnemyGunHelper enemyGunHelper;
        private GameObject gunHolder;

        [SerializeField] public float enemyMovementSpeed = 0.3f;
        [SerializeField] public float enemyRotationSpeed = 0.4f;

        private static float rotationMainSpeed;
        private static float movementMainSpeed;

        private bool isNeedToShoot;

        private bool needToRotate;
        private bool needToMove;



        public override void allActivity()
        {
            movement();
        }


        private void movement()
        {
            //always check the enemy direction
            checkTargetDirection();

            if ( getNeedToShoot() )
            {
                float distance = Vector3.Distance(transform.position, target.transform.position);
                
                
                if (distance <= EnemyGunHelper.maxDistanceToShoot)
                {
                    if (getEnemyDirection() == 1 || getEnemyDirection() == -1)
                    {
                        setNeedToRotate(true);
                        increaseRotationMainSpeed();
                        rotate90Degree();
                    }
                    else
                    {
                        stopRotationMainSpeed();
                        rotate90Degree();
                    }
                }
                else
                {
                    //move towards the enemy
                    rotateAndMoveTowardsEnemy();
                }
            }
        }

        public override void initAllInternalValues()
        {
            setIsAlive(true);
            setNeedToMove(false);
            setNeedToRotate(false);
            setNeedToShoot(false);
            
            enemyGunHelper    = GetComponent<EnemyGunHelper>();
            gunHolder         = enemyGunHelper.enemyGunHolder;
            rotationMainSpeed = 0;
            movementMainSpeed = 0;
        }


        //rotate and move towards the target
        public void rotateAndMoveTowardsEnemy()
        {
            Transform selfTransform = transform;

            Vector3 localPosition   = target.transform.position;
            var rotation  = Quaternion.LookRotation(localPosition - selfTransform.position);
            transform.rotation      = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * enemyRotationSpeed);
            
            transform.position = Vector3.MoveTowards(
                selfTransform.position, 
                localPosition, 
                enemyMovementSpeed
                );
        }
        
        
        private void checkTargetDirection()
        {
            float[] anfleArray  = HelperFunctions.getDirectionDifferenceAngle(target.transform, this.transform);
            float dotResult     = anfleArray[0];
            bool isBetweenAngle = (dotResult >= 125 && dotResult <= 185) ||
                                  (dotResult >= 0 && dotResult <= 45);
            
            if (dotResult >= 125 && dotResult <= 165 && isBetweenAngle )
            {
                setEnemyDirection(1f);
            }
            else if (dotResult >= 0 && dotResult <= 45 && isBetweenAngle )
            {
                setEnemyDirection(-1f);
            }
            
            setRayCastAndGetDirection();
        }
        
        
        private void setRayCastAndGetDirection()
        {
            for (int i = 0; i < 4; i++)
            {
                RaycastHit hit;
                GameObject gunPinter = gunHolder.transform.GetChild(i).gameObject;

                bool raycast         = Physics.Raycast(
                        gunPinter.transform.position,
                        gunPinter.transform.TransformDirection(Vector3.forward),
                        out hit,
                        Mathf.Infinity
                    );
                
                if (raycast)
                {
                    if (hit.transform.gameObject.CompareTag("Player"))
                    {
                        if (i == 0 || i == 1)
                        {
                            setEnemyDirection(-0.5f);
                        }
                        else
                        {
                            setEnemyDirection(0.5f);
                        }
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }


        public void setEnemyDirection(float value)
        {
            enemyDirection = value;
        }


        public float getEnemyDirection()
        {
            return enemyDirection;
        }

        public void setIsAlive(bool value)
        {
            isAlive = value;
        }

        public bool getIsAlive()
        {
            return isAlive;
        }

        public void setNeedToMove(bool value)
        {
            needToMove = value;
        }


        public bool getNeedToMove()
        {
            return needToMove;
        }


        public void setNeedToRotate(bool value)
        {
            needToRotate = value;
        }


        public bool getNeedToRotate()
        {
            return needToRotate;
        }

        //rotate 140 degree
        private void rotate90Degree()
        {
            Vector3 rotation = transform.position;
            Vector3 targetRotation = target.transform.position ;

            
            targetRotation = new Vector3(targetRotation.x - 140 , targetRotation.y , targetRotation.z);
            
            Quaternion lookRotation = Quaternion.LookRotation(targetRotation - rotation );

            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, rotationMainSpeed );
        }



        private void increaseRotationMainSpeed()
        {
            if ( rotationMainSpeed <= enemyRotationSpeed )
            {
                rotationMainSpeed += (float)(0.001 * Time.deltaTime );
            }
        }


        private void stopRotationMainSpeed()
        {
            if ( getEnemyDirection() == 0.5 || getEnemyDirection() == -0.5 )
            {
                rotationMainSpeed = 0.0f;
            } 
            else if (rotationMainSpeed > 0 )
            {
                rotationMainSpeed -= (float)(0.001 * Time.deltaTime);
            }
        }
        
        
        public  void setNeedToShoot(bool value)
        {
            isNeedToShoot = value;
        }
        
        
        public  bool getNeedToShoot()
        {
            return isNeedToShoot;
        }
        
    }
}