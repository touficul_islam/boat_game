﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unity.Boat
{
    public class WaveManager : MonoBehaviour
    {
        public static WaveManager instance;

        public float amplitute = 0.5f;
        public float length = 10f;
        public float speed = 1f;
        public float offset = 0f;

        private void Awake()
        {
            if (instance == null) instance = this;
            else if (instance != this) Destroy(this);
        }


        private void Update()
        {
            offset += Time.deltaTime * speed ;
        }


        public float getWaveHeight(float _x)
        {
            return amplitute * Mathf.Sin((_x / length) + offset) ;
        }
    }
}
