﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using Random = UnityEngine.Random;

namespace Unity.Boat
{
    public class EnvironmentController : MonoBehaviour
    {
        public float sunRotationSpeed = 0.4f;
        
        AudioSource seaSound;
        public Volume skyAndFogVolume;
        private HDRISky mainHdRISky;
        private float sunRotationPosition;

        void Start()
        {
            initEnv();
        }

        private void Update()
        {
            moveSun();
            pauseMenu();
        }


        public void pauseMenu()
        {
            if (InputHelper.pauseClick())
            {
                //stop time
                Time.timeScale = 0;
            }
        }

        private void moveSun()
        {
            if (sunRotationPosition >= 359.99)
            {
                sunRotationPosition = 0.01f;
            }
            
            sunRotationPosition += Time.deltaTime * sunRotationSpeed;

            float realAngle = HelperFunctions.unwrapAngle( sunRotationPosition );
            
            mainHdRISky.rotation.value = HelperFunctions.unwrapAngle( realAngle );
        }
        

        private void initEnv()
        {
            //playing sea sound
            seaSound = GetComponent<AudioSource>();
            //seaSound.Play(0);

            VolumeProfile volumeSharedProfile = skyAndFogVolume.sharedProfile;
            
            //HDRISky
            if (! volumeSharedProfile.TryGet<HDRISky>(out mainHdRISky) )
            {
                mainHdRISky = volumeSharedProfile.Add<HDRISky>(false);
            }
            
            //init sun rotation position
            // sunRotationPosition = Random.Range(0 , 350);
            sunRotationPosition = 357f;
        }
    }
}