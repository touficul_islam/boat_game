﻿using System;
using Unity.Boat;
using UnityEngine;

public class DestroyBall : MonoBehaviour
{
    public GameObject explotionPerticle;
    public GameObject fireFlame;
    private bool isDestroyed;
    
    private float start = 0;
    private float maxDuration = 10f;
    private readonly string[] objectTagsForDestroy = {"EnemyBoatCollaider", "PlayerColliders"};
    
    // Start is called before the first frame update
    void Start()
    {
        setIsDestroyed(false);
    }

    // Update is called once per frame
    void Update()
    {
        if ( ! getIsDestroyed())
        {
            selfDestroy();   
        }
    }


    private void selfDestroy()
    {
        start += Time.deltaTime;

        if( start >= maxDuration)
        {
            start = 0;
            setIsDestroyed(true);
            //Debug.Log("Destroy on time");
            Destroy(gameObject);
        }
    }


    public void setIsDestroyed(bool value)
    {
        isDestroyed = value;
    }


    public bool getIsDestroyed()
    {
        return isDestroyed;
    }

    private void OnTriggerEnter(Collider collision)
    {
        GameObject colldeObjectTag = collision.gameObject;
        
        if (Array.IndexOf(objectTagsForDestroy, colldeObjectTag.transform.tag ) != -1)
        {
            if (! getIsDestroyed())
            {
                GameObject explotionInstance = Instantiate(explotionPerticle, transform.position, Quaternion.Inverse(transform.rotation));
                GameObject fireInstance = Instantiate(fireFlame, transform.position, transform.rotation);
                
                explotionInstance.transform.SetParent(colldeObjectTag.transform);
                fireInstance.transform.SetParent(colldeObjectTag.transform);
                checkIfEnemyAndGetHimAngry(colldeObjectTag);
                setIsDestroyed(true);   
                Destroy(gameObject);
            }
        }
    }

    private void checkIfEnemyAndGetHimAngry(GameObject collision)
    {
        if (collision.CompareTag("EnemyBoatCollaider"))
        {
            EnemyAI enemyAI = collision.transform.root.GetComponent<EnemyAI>();

            if (enemyAI != null)
            {
                enemyAI.setNeedToShoot(true);
            }
        }
    }
}
