﻿using UnityEngine;

namespace Unity.Boat
{
    public class EnemyAIOld : MonoBehaviour
    {
        public GameObject target;
        public float speed = 2f;
        public float lookRotationSpeed = 0.1f;
        public float minDistance = 40f;
        public float maxDistance = 200f;

        private float rotationSpeed = 0.3f;
        private float enemyDirection;
        private bool isNeedToShoot;
        private EnemyGunHelper enemyGunHelper;
        private GameObject gunHolder;
        private int layerMusk = 9;
        private bool isAlive ;
        private bool inverse = false;

        private void Start()
        {
            setAisAlive(true);
            setIsNeedToShoot(false);
            enemyGunHelper = GetComponent<EnemyGunHelper>();
            gunHolder = enemyGunHelper.enemyGunHolder;
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (getIsAlive())
            {
                if (getIsNeedToShoot())
                {
                    //follow target
                    followTarget();
                    
                    checkTargetDirection();
                    checkEnemyAround();
                
                    
                }
            }
        }


        private void lookAtTarget()
        {
            Vector3 rotation = transform.position;
            Vector3 targetRotation = target.transform.position ;
            
            targetRotation = new Vector3(targetRotation.x - 90 , targetRotation.y , targetRotation.z);
            
            Quaternion lookRotation = Quaternion.LookRotation(targetRotation - rotation );

            transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, rotationSpeed * Time.deltaTime);
        }
        

        private void followTarget()
        {
            if (Vector3.Distance(transform.position, target.transform.position) >= minDistance)
            {
                Vector3 position = target.transform.position;
                Vector3 position1 = transform.position;
                Vector3 targetPosition = new Vector3(position.x, position1.y, position.z);

                position1 = Vector3.MoveTowards(position1, targetPosition, speed * Time.deltaTime);
                transform.position = position1;

            
                HelperFunctions.lookAtTarget(transform, target.transform, lookRotationSpeed);
            }
            else
            {
                lookAtTarget();
            }
        }

        

        private void checkTargetDirection()
        {
            float[] anfleArray = HelperFunctions.getDirectionDifferenceAngle(target.transform, this.transform);
            float dotResult = anfleArray[0];
            bool isBetweenAngle = (dotResult >= 125 && dotResult <= 185) ||
                                  (dotResult >= 0 && dotResult <= 45);
            
            if (dotResult >= 125 && dotResult <= 165 && isBetweenAngle )
            {
                setEnemyDirection(1f);
            }
            else if (dotResult >= 0 && dotResult <= 45 && isBetweenAngle )
            {
                setEnemyDirection(-1f);
            }
            
            setRayCastAndGetDirection();
        }


        private void setRayCastAndGetDirection()
        {
            for (int i = 0; i < 4; i++)
            {
                RaycastHit hit;
                GameObject gunPinter = gunHolder.transform.GetChild(i).gameObject;
                
                
                if (Physics.Raycast(gunPinter.transform.position, gunPinter.transform.TransformDirection(Vector3.forward), out hit,  Mathf.Infinity))
                {
                    if (hit.transform.gameObject.CompareTag("Player"))
                    {
                        if (i == 0 || i == 1)
                        {
                            setEnemyDirection(-0.5f);
                        }
                        else
                        {
                            setEnemyDirection(0.5f);
                        }
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }


        private void checkEnemyAround()
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, 15, layerMusk);

            if (colliders.Length > 0)
            {
                setIsNeedToShoot(true);
                enemyGunHelper.setFinishReload(true);
            }
        }
        
        
        public void setAisAlive(bool value)
        {
            isAlive = value;
        }


        public bool getIsAlive()
        {
            return isAlive;
        }


        public void setEnemyDirection(float value)
        {
            enemyDirection = value;
        }


        public float getEnemyDirection()
        {
            return enemyDirection;
        }


        public void setIsNeedToShoot(bool value)
        {
            if (value == getIsNeedToShoot())
            {
                return;
            }
            isNeedToShoot = value;
        }


        public bool getIsNeedToShoot()
        {
            return isNeedToShoot;
        }
    }
}
