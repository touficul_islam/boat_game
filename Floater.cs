﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unity.Boat
{
    public class Floater : MonoBehaviour
    {
        public Rigidbody rigidbody;

        public float depthBeforeSubmerge = 7.44f;
        public float displacementAmount = 7.44f;
        public float floaterCount = 4f;
        public float waterDrag = 10f;
        public float waterAngularDrag = 18f;



        // Start is called before the first frame update
        private void Update()
        {
            floatObject();
        }


        private void floatObject()
        {
            Vector3 position = transform.position;
            rigidbody.AddForceAtPosition(Physics.gravity / floaterCount, position, ForceMode.Acceleration);

            float waveHeight = WaveManager.instance.getWaveHeight(position.x);

            // Debug.Log(getWaterDrag());

            if (transform.position.y < waveHeight)
            {
                float displacementMultiplier = Mathf.Clamp01((waveHeight - transform.position.y) / depthBeforeSubmerge) * displacementAmount;
                rigidbody.AddForceAtPosition(new Vector3(0f, Mathf.Abs(Physics.gravity.y) * displacementMultiplier, 0f), position, ForceMode.Acceleration);
                rigidbody.AddForce(displacementMultiplier * -rigidbody.velocity * getWaterDrag() * Time.fixedDeltaTime, ForceMode.VelocityChange);
                rigidbody.AddTorque(displacementMultiplier * -rigidbody.angularVelocity * waterAngularDrag * Time.fixedDeltaTime, ForceMode.VelocityChange);
            }
        }


        public void setWaterDrag(float wDrag)
        {
            waterDrag = wDrag;
        }


        public float getWaterDrag()
        {
            return waterDrag;
        }
    }
}
