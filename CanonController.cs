﻿using UnityEngine;

namespace Unity.Boat{
    public class CanonController : MonoBehaviour
    {
        private Transform shootPoint;
        private CanonShoot canonShoot;
        private DrawProjection drawProjection;

        private float rotationYAxis;
        private float rotationXAxis;

        private float velocityX = 0.0f;
        private float velocityY = 0.0f;

        private bool isShoot = false;
        private bool isDraw = false;

        public float xSpeed = 20.0f;
        public float ySpeed = 20.0f;
        public float yMinLimit = -90f;
        public float yMaxLimit = 90f;
        public float xMaxLimit = 90f;
        public float xMinLimit = -90f;
        public float rotateSpeed = 2f;
        public float smoothTime = 2f;
        public float blastPower = HelperFunctions.canonBallVelocity;
        public bool inverse = false;
        
        


        // Start is called before the first frame update
        void Start()
        {
            shootPoint = transform.GetChild(0).transform;
            canonShoot = GetComponent<CanonShoot>();
            drawProjection = GetComponent<DrawProjection>();
        }

        // Update is called once per frame
        void Update()
        {
            if(shootPoint != null)
            {
                if ( getIsDraw())
                {
                    drawProjection.drawLine();
                    rotateCanon();
                }
                else
                {
                    drawProjection.destroyLine();
                }
                
                if (getIsShoot())
                {
                    canonShoot.shoot();
                }
            }
        }


        public void rotateCanon()
        {
            if (inverse)
            {
                velocityX += xSpeed * -1 * InputHelper.getMouseX() *  0.02f;
                velocityY += ySpeed * -1 * InputHelper.getMouseY() * 0.02f;
            }
            else
            {
                velocityX += xSpeed * InputHelper.getMouseX() *  0.02f;
                velocityY += ySpeed * InputHelper.getMouseY() * 0.02f;
            }

            rotationYAxis += velocityX;
            rotationXAxis -= velocityY;

            rotationXAxis = HelperFunctions.ClampAngle(rotationXAxis, yMinLimit, yMaxLimit);
            rotationYAxis = HelperFunctions.ClampAngle(rotationYAxis, xMinLimit, xMaxLimit);


            Quaternion toRotation = Quaternion.Euler(rotationXAxis, rotationYAxis, 0);
            Quaternion rotation = toRotation;


            shootPoint.localRotation = rotation;

            velocityX = Mathf.Lerp(velocityX, 0, Time.deltaTime * smoothTime);
            velocityY = Mathf.Lerp(velocityY, 0, Time.deltaTime * smoothTime);
        }


        public Transform getShootPosition()
        {
            return shootPoint;
        }


        public void setIsShoot(bool value)
        {
            isShoot = value;
        }


        public bool getIsShoot()
        {
            return isShoot;
        }


        public void setIsDraw(bool value)
        {
            isDraw = value;
        }


        public bool getIsDraw()
        {
            return isDraw;
        }
    }
}
