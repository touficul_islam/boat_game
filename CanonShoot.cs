﻿using System.Collections;
using UnityEngine;

namespace Unity.Boat
{
    public class CanonShoot : MonoBehaviour
    {
        private AudioSource canonBallShootSound;
        public GameObject canonBall;
        private CanonController canonController;
        private ParticleSystem shootExplosion;

        // Use this for initialization
        void Start()
        {
            canonController = GetComponent<CanonController>();
            canonBallShootSound = transform.GetChild(0).gameObject.GetComponent<AudioSource>();
            shootExplosion = transform.GetChild(0).transform.GetChild(0).transform.gameObject
                .GetComponent<ParticleSystem>();

        }

        public void shoot()
        {
            if (canonBallShootSound != null)
            {
                canonBallShootSound.Play();    
            }

            if (shootExplosion != null)
            {
                shootExplosion.Play();
            }
            
            GameObject canonBallToShoot = HelperFunctions.makeCanonBall(canonBall);
            canonBallToShoot.transform.TransformDirection(-Vector3.forward);
            canonBallToShoot.transform.position = canonController.getShootPosition().position;
            canonBallToShoot.transform.rotation = canonController.getShootPosition().rotation;
            canonBallToShoot.GetComponent<Rigidbody>().velocity = canonBallToShoot.transform.TransformDirection(Vector3.up * canonController.blastPower);
            
        }
    }
}