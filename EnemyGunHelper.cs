﻿using System.Collections;
using UnityEngine;

namespace Unity.Boat
{
    public class EnemyGunHelper : MonoBehaviour
    {
        public GameObject enemyGunHolder;
        public GameObject canonBall;

        private EnemyAI enemyAI;
        private float timer = 0f;
        private bool finishReload = true;
        
        public static float maxDistanceToShoot = 100f; 

        private void Start()
        {
            enemyAI = GetComponent<EnemyAI>();
            setFinishReload(true);
        }

        private void Update()
        {
            if (enemyAI.getNeedToShoot())
            {
                reloadAndShoot();
            }
        }


        private void reloadAndShoot()
        {
            timer += Time.deltaTime;
            int seconds = (int)(timer % 60);
            
            if (seconds >= HelperFunctions.reloadTime )
            {
                setTimer(0);
                canonShoot();
                setFinishReload(true);
            }
            else
            {
                setFinishReload(false);
            }
        }


        public void setFinishReload(bool value)
        {
            finishReload = value;
        }


        public bool getFinishReload()
        {
            return finishReload;
        }


        public void setTimer(float time)
        {
            timer = time;
        }


        public float getTimer()
        {
            return timer;
        }

        private void canonShoot()
        {
            float checkEnemyDirection = enemyAI.getEnemyDirection();
            
            
            if (checkEnemyDirection == -0.5f)
            {
                activateCanon(0, 1);
                deactivateCanon(2, 3);
            }
            else if (checkEnemyDirection == 0.5f)
            {
                activateCanon(2, 3); 
                deactivateCanon(0, 1);
            }
        }

        private void activateCanon(int start, int end)
        {
            for (int i = start; i <= end; i++)
            {
                GameObject pointerGameObject = enemyGunHolder.transform.GetChild(i).gameObject; 
                Transform gunPinter = pointerGameObject.transform;
                AudioSource canonshootSound = pointerGameObject.GetComponent<AudioSource>();
                
                if (canonBall != null)
                {
                    GameObject canonBallToShoot = HelperFunctions.makeCanonBall(canonBall);
                
                    if (canonshootSound != null)
                    {
                        canonshootSound.Play();
                    }

                    
                    throwCanonBall(canonBallToShoot, gunPinter, i);
                    startParticles(pointerGameObject.transform.GetChild(0).gameObject.GetComponent<ParticleSystem>());
                }
                else
                {
                    return;
                }
            }
        }

        private void deactivateCanon(int start, int end)
        {
            for (int i = start; i < end; i++)
            {
                Transform gunPinter = enemyGunHolder.transform.GetChild(i).gameObject.transform;
                if (i == 0 || i == 1)
                {
                    gunPinter.localRotation = Quaternion.Euler(0f, 90f, 0f);
                }
                else
                {
                    gunPinter.localRotation = Quaternion.Euler(0f, -90f, 0f);
                }
            }
        }

        private void rotateGunPointerToTargetAngle(Transform objectToRotate, int index)
        {
            Vector3 relativePos = enemyAI.target.transform.position - objectToRotate.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos);
            objectToRotate.rotation = rotation;
            Vector3 eulerAngles = objectToRotate.eulerAngles;
            Vector3 tempRot = eulerAngles;
            
            //This line of code is so that we can point towards the target position, while also pointing to the firing angle
            tempRot.x = eulerAngles.x-HelperFunctions.getAngle(objectToRotate, enemyAI.target.transform);
            eulerAngles = tempRot;
            objectToRotate.eulerAngles = eulerAngles;
        }


        private void throwCanonBall(GameObject canonBallToShoot ,Transform gunPinter, int canonIndex)
        {
            rotateGunPointerToTargetAngle(gunPinter, canonIndex);
            canonBallToShoot.transform.position = gunPinter.position;
            canonBallToShoot.transform.rotation = gunPinter.rotation;
            canonBallToShoot.GetComponent<Rigidbody>().velocity = canonBallToShoot.transform.TransformDirection(Vector3.forward * HelperFunctions.canonBallVelocity  );
        }



        private void startParticles(ParticleSystem particleSystem)
        {
            if (particleSystem != null)
            {
                particleSystem.Play();
            }
        }
    }
}