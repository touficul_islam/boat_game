﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unity.Boat
{
    public class BoatController : MonoBehaviour
    {
        public Transform spanker;
        public CameraMovement cameraMovement;
        public Transform[] cameraFollowTarget;

        private Transform floaters;
        
        public float movementSpeed = 40f;
        public float rotateSpeed = 10f;
        public float spankerRotationSpeed = 11.45f;
        public float rotateTorQue = 4f;
        

        private float maxSpankerRotate = 118f;
        private Vector3 moveVelocity;
        private float spankerRotation = 0;
        private float currentSpeed;
        [SerializeField]
        private float maxSpeed = 8;
        [SerializeField]
        private float minSpeed = 2;
        private Rigidbody boatRigidBody;
        private bool moveBool = false;

        private Transform shipWheel;
        private float shipWheelRotation = 0;
        private float shipWheelRotationSpeed = 0;
        
        

        // Start is called before the first frame update
        void Start()
        {
            cameraMovement.setTarget(cameraFollowTarget[0]);
            
            currentSpeed = minSpeed;
            moveVelocity = Vector3.zero;
            boatRigidBody = GetComponent<Rigidbody>();

            floaters = transform.Find("floaters");
            
            shipWheel = transform.GetChild(0).gameObject.transform.GetChild(1).gameObject.transform.GetChild(0).transform;
        }

        public void FixedUpdate()
        {
            if( moveBool ) move();
            else
            {
                moveVelocity = Vector3.zero;
                currentSpeed = 0;
            }
            
            changeSpeed();
            boatRotate();
            changeCameraFollowTarget();
            rotateShipWheel();
        }

        // Update is called once per frame
        void Update()
        {
            changeMoveStatus();
        }


        //change camera position on speed up
        public void changeCameraFollowTarget()
        {
            if (getCurrentSpeed() >= maxSpeed) cameraMovement.setTarget(cameraFollowTarget[1]);
            else cameraMovement.setTarget(cameraFollowTarget[0]);
        }


        //rotate the boat
        public void boatRotate()
        {
            if (Input.GetKey(KeyCode.D))
            {
                transform.Rotate(new Vector3(0f, rotateSpeed * Time.deltaTime, 0f));
                boatRigidBody.AddTorque(new Vector3(0, 0, rotateTorQue), ForceMode.Force);
            }

            if (Input.GetKey(KeyCode.A))
            {
                transform.Rotate(new Vector3(0f, -rotateSpeed * Time.deltaTime, 0f));
                boatRigidBody.AddTorque(new Vector3(0, 0, -rotateTorQue), ForceMode.Force);
            }

            if( moveBool ) transform.Translate(moveVelocity.normalized * Time.deltaTime * currentSpeed);
            else transform.Translate(moveVelocity.normalized * Time.deltaTime );

            rotateSpanker();
        }

        
        //rotate ship wheel
        private void rotateShipWheel()
        {

            if ( Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) )
            {
                if (Input.GetKey(KeyCode.A))
                {
                    shipWheelRotationSpeed = 1;
                }
                else if (Input.GetKey(KeyCode.D))
                {
                    shipWheelRotationSpeed = -1;
                }
                
                shipWheelRotation += shipWheelRotationSpeed ;

                shipWheelRotation = Mathf.Clamp(shipWheelRotation, -80, 80);
            }
            else
            {
                //reverse back to 0 0 0 position
                if (shipWheelRotation != 0)
                {
                    if( shipWheelRotation > 0 )
                    {
                        shipWheelRotation--;
                    }
                    else
                    {
                        shipWheelRotation++;
                    }
                }
            }
            shipWheel.transform.localRotation = Quaternion.Euler(0, shipWheelRotation, 0);
        }
        
        
        //boat move
        public void move()
        {
            moveVelocity.z = movementSpeed;
            
            transform.Translate(moveVelocity.normalized * Time.deltaTime * currentSpeed);
        }



        //rotate boat spanker with left right movement
        private void rotateSpanker()
        {
            if (Input.GetKey(KeyCode.D) && spankerRotation != maxSpankerRotate )
            {
                spanker.Rotate( Vector3.up * Time.deltaTime * spankerRotationSpeed );
                spankerRotation++;
            }

            if (Input.GetKey(KeyCode.A) && spankerRotation != -maxSpankerRotate)
            {
                spanker.Rotate(-Vector3.up * Time.deltaTime * spankerRotationSpeed);
                spankerRotation--;
            }
        }


        //change boat speed
        public void changeSpeed()
        {
            if (Input.GetKeyDown(KeyCode.LeftShift) && getCurrentSpeed() < maxSpeed)
            {
                currentSpeed += 2;
            }
            else if (Input.GetKeyDown(KeyCode.S) && getCurrentSpeed() >= minSpeed)
            {
                currentSpeed -= 2;
            }
        }


        //change boat movement status
        public void changeMoveStatus()
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                moveBool = !moveBool;
            }

            if (moveBool)
            {
                if (getCurrentSpeed() <= 0) setCurrentSpeed(minSpeed);
                if (getCurrentSpeed() >= maxSpeed) setCurrentSpeed(maxSpeed);
            }
            else setCurrentSpeed(0);
        }



        public void setCurrentSpeed(float value)
        {
            currentSpeed = value;
        }


        public float getCurrentSpeed()
        {
            return currentSpeed;
        }

        public float getMaxSpeed()
        {
            return maxSpeed;
        }


        public void setMaxSpeed(float value)
        {
            maxSpeed = value;
        }
    }
}
