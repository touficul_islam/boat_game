﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Unity.Boat
{
    public class DrawProjection : MonoBehaviour
    {
        CanonController canonController;
        LineRenderer lineRenderer;

        public LayerMask collidableLayer;

        public float numberOfPoints = 60;
        public float timeBetweenPoints = 0.1f;


        // Use this for initialization
        void Start()
        {
            canonController = GetComponent<CanonController>();
            lineRenderer = GetComponent<LineRenderer>();
        }
        

        public void drawLine()
        {
            lineRenderer.positionCount = (int)numberOfPoints;

            List<Vector3> points = new List<Vector3>();
            Vector3 startPosition = canonController.getShootPosition().position;
            Vector3 startVelocity = canonController.getShootPosition().up * canonController.blastPower;

            for( float i = 0; i < numberOfPoints; i += timeBetweenPoints)
            {
                Vector3 newPoint = startPosition + (i * startVelocity );
                newPoint.y = startPosition.y + startVelocity.y * i + (Physics.gravity.y * 0.5f * Mathf.Pow(i, 2));
                points.Add( newPoint );

                if( Physics.OverlapSphere(newPoint, 2, collidableLayer).Length > 0)
                {
                    lineRenderer.positionCount = points.Count;
                    break;
                }
            }

            lineRenderer.SetPositions(points.ToArray());
        }


        public void destroyLine()
        {
            lineRenderer.positionCount = 0;
        }
    }
}