﻿using UnityEditor;
using UnityEngine;

namespace Unity.Boat
{
    public class InputHelper
    {
        public static float getHorizontal()
        {
            return Input.GetAxis("Horizontal");
        }


        public static float getVertical()
        {
            return Input.GetAxis("Vertical");
        }


        public static float getMouseX()
        {
            return Input.GetAxis("Mouse X");
        }


        public static float getMouseY()
        {
            return Input.GetAxis("Mouse Y");
        }


        public static bool pauseClick()
        {
            return Input.GetKeyDown(KeyCode.Escape);
        }
    }
}