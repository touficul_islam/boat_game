﻿using System.Collections;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace Unity.Boat
{
    public class CameraMovement : MonoBehaviour
    {
        //Object want to follow
        private Transform target;
        //Set to z -10
        public Vector3 offset;

        //camera movement
        public float distance = 2.0f;
        public float xSpeed = 15.0f;
        public float ySpeed = 15.0f;
        
        public float xMaxLimit = 50.0f;
        public float xMinLimit = -20.0f;
        
        //shake effect
        public float effectDuration = 0.4f;

        [Range(0.1f, 1.0f)]
        public float shakestrength = 0.4f;

        public float smoothTime = 2f;
        private float rotationYAxis = 0.0f;
        private float rotationXAxis = 0.0f;
        private float velocityX = 0.0f;
        private float velocityY = 0.0f;

        //How much delay on the follow
        [Range(1, 10)]
        public float smoothing;

        private void Start()
        {
            Vector3 angles = transform.root.transform.eulerAngles;
            rotationYAxis = angles.y;
            rotationXAxis = angles.x;
        }

        private void Awake()
        {
            moveCamera();
        }

        private void FixedUpdate()
        {
            Follow();
            moveCamera();
        }
        

        private void moveCamera()
        {
            if (target)
            {
                velocityX += xSpeed * InputHelper.getMouseX() * distance * 0.02f;
                velocityY += ySpeed * InputHelper.getMouseY() * 0.02f;
                
                rotationXAxis -= velocityY;
                rotationYAxis += velocityX;

                rotationXAxis = HelperFunctions.ClampAngle(rotationXAxis, xMinLimit, xMaxLimit);

                transform.root.transform.localRotation = Quaternion.Euler(rotationXAxis , rotationYAxis , 0) ;

                velocityX = Mathf.Lerp(velocityX, 0, Time.deltaTime * smoothTime);
                velocityY = Mathf.Lerp(velocityY, 0, Time.deltaTime * smoothTime);
            }
        }

        public IEnumerator shakeMeBaby()
        {
            Vector3 currentPosition = transform.localPosition;

            float timeHasGone = 0f;

            while (timeHasGone < effectDuration)
            {
                float x = Random.Range(-1f, 1f) * shakestrength;
                float y = Random.Range(-1f, 1f) * shakestrength;

                timeHasGone += Time.deltaTime;
                transform.localPosition =  Vector3.Slerp(new Vector3(x, y, currentPosition.z), currentPosition, timeHasGone);

                yield return null;
            }

            transform.localPosition = currentPosition;
        }
        

        void Follow()
        {
            Vector3 targetPosition = target.position + offset;
            Vector3 smoothPosition = Vector3.Slerp(transform.position, targetPosition, smoothing * Time.fixedDeltaTime);
            
            transform.root.transform.position = smoothPosition;
        }


        public void setTarget(Transform newTarget)
        {
            target = newTarget;
        }


        public Transform getTarget()
        {
            return target;
        }
    }
}
