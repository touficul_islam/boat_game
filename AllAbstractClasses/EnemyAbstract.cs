﻿using System;
using UnityEngine;

namespace Unity.Boat
{
    public abstract class EnemyAbstract : MonoBehaviour
    {
        public abstract void allActivity();

        public abstract void initAllInternalValues();


        private void Awake()
        {
            initAllInternalValues();
        }

        private void Update()
        {
            allActivity();
        }
    }
}