﻿using System;
using System.Collections;
using UnityEngine;

namespace Unity.Boat
{
    public class HelperFunctions : MonoBehaviour
    {
        public static float canonBallVelocity = 40f;
        public static int reloadTime = 3;
        
        public static bool IsBetween(float testValue, float bound1, float bound2)
        {
            return (testValue >= Mathf.Min(bound1, bound2) && testValue <= Mathf.Max(bound1, bound2));
        }


        public static float ClampAngle(float angle, float min, float max)
        {
            if (angle < -360F) angle += 360F;
            if (angle > 360F) angle -= 360F;
            return Mathf.Clamp(angle, min, max);
        }

        
        public static float ClampAngleTwo(float angle, float min, float max)
        {
            if (angle < 90 || angle > 270)
            {       // if angle in the critic region...
                if (angle > 180) angle -= 360;  // convert all angles to -180..+180
                if (max > 180) max -= 360;
                if (min > 180) min -= 360;
            }
            angle = Mathf.Clamp(angle, min, max);
            if (angle < 0) angle += 360;  // if angle negative, convert to 0..360
            return angle;
        }
        
        
        public static float normalizeAngle(float a)
        {
            return a - 180f * Mathf.Floor((a + 180f) / 180f);
        }

        public static GameObject makeCanonBall(GameObject reference)
        {
            return Instantiate(reference);
        }

        public static float angleDifference(Transform rotationOne, Transform rotationTwo)
        {
            Vector3 dir = rotationTwo.position - rotationOne.position;
            dir = rotationTwo.InverseTransformDirection(dir);
            return dir.y * Mathf.Rad2Deg;
        }


        public static float[] getDirectionDifferenceAngle(Transform target, Transform currentPosition)
        {
            float cosARad = Vector3.Dot(
                (target.position-currentPosition.position).normalized, currentPosition.forward
                );
            float[] angleAndRad = { Mathf.Acos(cosARad) * Mathf.Rad2Deg, cosARad };
            return angleAndRad;
        }


        public static void throwProjectile(GameObject objectToThrow, float speed)
        {
            objectToThrow.GetComponent<Rigidbody>().AddForce(objectToThrow.transform.forward * speed, ForceMode.Force);
        }
        
        
        public static float calculateProjectileAngleForADistance(Transform objectOne, Transform objectTwo, float velocity)
        {
            var position = objectOne.position;
            
            float y = position.y - objectTwo.position.y;
            float x = position.x - objectTwo.position.x;
            var v = velocity;
            var g = -Physics.gravity.y;
            var sqrt = (v*v*v*v) - (g * (g * (x*x) + 2 * y * (v*v)));
            
            if (sqrt < 0)
            {
                Debug.Log("No Solution");
                return 0;
            }
            
            sqrt = Mathf.Sqrt(sqrt);
            return Mathf.Atan(((v*v) + sqrt) / (g*x)) ;
            
           
        }
        
        
        public static float getAngle( Transform objectOne, Transform objectTwo)
        {
            float distance = Vector3.Distance(objectOne.position, objectTwo.position);
            float gravity = 9.81f;
            float angle = 0.5f*(Mathf.Asin ((gravity * distance) / (canonBallVelocity * canonBallVelocity)));
            return angle*Mathf.Rad2Deg;
     
        }
        
        public static GameObject FindComponentInChildWithTag(GameObject parent, string tag) {
            Transform t = parent.transform;
            foreach(Transform tr in t)
            {
                if(tr.name == tag)
                {
                    return tr.gameObject;
                }
            }
            return null;
        }


        public static void lookAtTarget(Transform position, Transform targetPosition, float speed, bool inverse = true)
        {
            Vector3 relativePos = targetPosition.position - position.position;
            Quaternion toRotation = Quaternion.LookRotation(relativePos);

            if (inverse)
            {
                toRotation = Quaternion.Inverse(toRotation);
            }
            
            position.rotation = Quaternion.Slerp(position.rotation, toRotation, speed * Time.deltaTime);
        }
        
        public static float unwrapAngle(float angle)
        {
            if(angle >=0)
                return angle;
 
            angle = -angle%360;
 
            return 360-angle;
        }

        public static void pauseAudio(AudioSource audioSource)
        {
            audioSource.Pause();
        }
        
        
        public static void stopAudion(AudioSource audioSource)
        {
            audioSource.Stop();
        }
    }
    
}